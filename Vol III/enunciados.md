# Enunciados ejercicios Python volumen III

## 001 Inventario

_Nivel_ [sin definir]

### Especificaciones

Crea un programa que permita la gestión del inventario de datos almacenados en diccionarios y listas. Para ello el programa deberá proporcionar funciones que realicen las siguientes tareas

1. Función: _create_inventary(items)_. Crea un diccionario que haga las veces de inventario

    - **Entrada**: lista -> ["agua", "pan", "leche", "agua", "leche", "cereales", "agua"]
    - **Salida**: diccionario -> { "agua": 3, "pan":1, "leche": 2, "cereales": 1}


2. Función: _add_items(inventary, items)_. Añade una listas de elementos al inventario

    - **Entrada**: inventary -> { "agua": 3, "pan":1, "leche": 2, "cereales": 1}, ["agua", "pan"]
    - **Salida**: items -> { "agua": 4, "pan":2, "leche": 2, "cereales": 1}, 


3. Función: _reduce_items(inventary, items)_. Reduce el inventario según los elementos de una lista.

    - **Entrada**: inventary -> { "agua": 3, "pan":1, "leche": 2, "cereales": 1}, ["agua", "pan"]
    - **Salida**: items -> { "agua": 2, "pan": 1, "leche": 2, "cereales": 1}, 


4. Función: _remove_item(inventary, item)_. Elimina un elemento del inventario

    - **Entrada**: { "agua": 3, "pan":1, "leche": 2, "cereales": 1}, "agua"
    - **Salida**: { "pan": 1, "leche": 2, "cereales": 1}, 

5. Función: _show_inventario(inventary)_. Muestra una lista de tuplas item, valor siempre que el numoer de elementos en el inventario sea mayor de 0

    - **Entrada**: { "agua": 3, "pan":1, "leche": 2, "cereales": 1, "salchichon": 0}, 
Salida: [("agua", 3), ("pan", 1), ("leche", 2), ("cereales", 1)]
    - **Salida**:  [("agua", 3), ("pan", 1), ("leche", 2), ("cereales", 1)

